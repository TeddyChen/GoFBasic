﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;
using System.Collections.Generic;

namespace Tw.Teddysoft.Gof.Command.Exercise
{
    public class Server 
    {
	    IList<Client<Door>> doors = new List<Client<Door>>();
	    IList<Client<Window>> windows = new List<Client<Window>>();
	    IList<Client<Thermal>> thermals = new List<Client<Thermal>>();

        public void monitor(){
	        foreach(Client<Door> door in doors){
	            if ("open".Equals(door.getCheckCommand().getDoorStatus() )){
	                sendAlert("門被打開" );
	            }
	        }
	        foreach(Client<Window> window in windows){
	            if (window.getCheckCommand().isOpen())
	                sendAlert("窗戶被開啟");
	            
	            if (window.getCheckCommand().isBroken())
	                sendAlert("窗戶被打破");
	        }
	        foreach(Client<Thermal> thermal in thermals){
	            if (thermal.getCheckCommand().isOverheat())
	                sendAlert("溫度過熱");
	        }
        }
	    public void sendAlert(String msg){
            Console.WriteLine("發現問題並通知保全人員: " + msg);
        }
	    
	    public void addDoor(Client<Door> client)
        {
            doors.Add(client);
        }
	    
	    public void addWindow(Client<Window> client)
        {
            windows.Add(client);   
	    }
	    
	    public void addThermal(Client<Thermal> client)
        {
            thermals.Add(client);
        }
    }

}
