﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;
using NUnit.Framework;

namespace Tw.Teddysoft.Gof.State.Exercise
{
    [TestFixture]
    public class HostStateTransitionTest
    {
        class DownCommand : Command
        {
            public CheckResult execute()
            {
                return CheckResult.DOWN;
            }
        }
        class UpCommand : Command
        {
            public CheckResult execute()
            {
                return CheckResult.UP;
            }
        }
        [Test]
        public void test_down_transition()
        {
            Host host = new Host(new DownCommand());
            Assert.AreEqual(HostState.UP_HARD, host.getState());
            host.check();
            Assert.AreEqual(HostState.DOWN_SOFT, host.getState());
            host.check();
            Assert.AreEqual(HostState.DOWN_SOFT, host.getState());
            host.check();
            Assert.AreEqual(HostState.DOWN_HARD, host.getState());
            host.check();
            Assert.AreEqual(HostState.DOWN_HARD, host.getState());
            host.check();
            Assert.AreEqual(HostState.DOWN_HARD, host.getState());
            Assert.AreEqual(3, host.getAttempt());

            host.changeState(HostState.UP_SOFT);
            Assert.AreEqual(HostState.UP_SOFT, host.getState());
            host.check();
            Assert.AreEqual(HostState.DOWN_HARD, host.getState());
        }

        [Test]
        public void test_up_transition()
        {
            Host host = new Host(new UpCommand());
            host.changeState(HostState.DOWN_HARD);
            Assert.AreEqual(HostState.DOWN_HARD, host.getState());
            host.check();
            Assert.AreEqual(HostState.UP_SOFT, host.getState());
            host.check();
            Assert.AreEqual(HostState.UP_SOFT, host.getState());
            host.check();
            Assert.AreEqual(HostState.UP_HARD, host.getState());
            host.check();
            Assert.AreEqual(HostState.UP_HARD, host.getState());
            host.check();
            Assert.AreEqual(HostState.UP_HARD, host.getState());
            Assert.AreEqual(3, host.getAttempt());

            host.changeState(HostState.DOWN_SOFT);
            Assert.AreEqual(HostState.DOWN_SOFT, host.getState());
            host.check();
            Assert.AreEqual(HostState.UP_HARD, host.getState());
        }
	}
}
