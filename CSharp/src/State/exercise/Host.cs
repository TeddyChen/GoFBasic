﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;

namespace Tw.Teddysoft.Gof.State.Exercise
{
    public class Host
    {
        private HostState currentState = HostState.UP_HARD;
        private Command checkCommand;
        private int maxAttempt = 3;
        private int attempt = 0;
        public Host(Command command)
        {
            checkCommand = command;
        }
        public void setCommand(Command command)
        {
            checkCommand = command;
        }
        public void check()
        {
            CheckResult result = checkCommand.execute();
            switch (result)
            {
                case CheckResult.UP:
                    switch (currentState)
                    {
                        case HostState.UP_HARD:
                            attempt++;
                            break;
                        case HostState.UP_SOFT:
                            attempt++;
                            if (attempt >= maxAttempt)
                                changeState(HostState.UP_HARD);
                            break;
                        case HostState.DOWN_HARD:
                            changeState(HostState.UP_SOFT);
                            break;
                        case HostState.DOWN_SOFT:
                            changeState(HostState.UP_HARD);
                            break;
                    }
                    break;
                case CheckResult.DOWN:
                    switch (currentState)
                    {
                        case HostState.UP_HARD:
                            changeState(HostState.DOWN_SOFT);
                            break;
                        case HostState.UP_SOFT:
                            changeState(HostState.DOWN_HARD);
                            break;
                        case HostState.DOWN_HARD:
                            attempt++;
                            break;
                        case HostState.DOWN_SOFT:
                            attempt++;
                            if (attempt >= maxAttempt)
                            {
                                changeState(HostState.DOWN_HARD);
                            }
                            break;
                    }
                    break;
            }
        }
        public void powerOff()
        {
            switch (currentState)
            {
                case HostState.UP_HARD:
                    doGracefulPowerOff();
                    break;
                case HostState.UP_SOFT:
                    doPowerOff(60);
                    break;
                case HostState.DOWN_HARD:
                    Console.WriteLine("Cannot power off under down hard state.");
                    break;
                case HostState.DOWN_SOFT:
                    Console.WriteLine("Cannot power off under down soft state.");
                    break;
            }
        }
        public void powerOn()
        {
            switch (currentState)
            {
                case HostState.UP_HARD:
                    Console.WriteLine("Cannot power on under up hard state.");
                    break;
                case HostState.UP_SOFT:
                    Console.WriteLine("Cannot power on under up soft state.");
                    break;
                case HostState.DOWN_HARD:
                    doPowerOn();
                    break;
                case HostState.DOWN_SOFT:
                    Console.WriteLine("Cannot power on under down soft state.");
                    break;
            }
        }
        public String diagnostic()
        {
            switch (currentState)
            {
                case HostState.UP_HARD:
                    return inBandDiagnostic();
                case HostState.UP_SOFT:
                    String result = inBandDiagnostic();
                    if ("" == result)
                        return outOfBandDiagnostic();
                    else
                        return result;
                case HostState.DOWN_HARD:
                    return outOfBandDiagnostic();
                case HostState.DOWN_SOFT:
                    return outOfBandDiagnostic();
                default:
                    throw new Exception
                    ("Unsupported state: " + currentState);
            }
        }
        public int getAttempt()
        {
            return attempt;
        }
        public HostState getState()
        {
            return currentState;
        }
        public void changeState(HostState newState)
        {
            resetAttempt();
            currentState = newState;
        }
        public int getMaxAttempt()
        {
            return maxAttempt;
        }
        private void resetAttempt()
        {
            attempt = 1;
        }
        private void doGracefulPowerOff()
        {
            Console.WriteLine("Power off gracefully.");
        }
        private void doPowerOff(int delay)
        {
            Console.WriteLine("Power off after " + delay + " second(s).");
        }
        private void doPowerOn()
        {
            Console.WriteLine("Power On.");
        }
        private String inBandDiagnostic()
        {
            return "Diagnostic via the remote agent.";
        }
        private String outOfBandDiagnostic()
        {
            return "Diagnostic via IPMI.";
        }
    }
}
