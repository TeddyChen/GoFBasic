﻿/*
 * Copyright 2017 TeddySoft Technology. 
 *
 */
using System;
using System.Collections.Generic;

namespace Tw.Teddysoft.Gof.Composite.Exercise
{
    public abstract class Bullet 
	{
        public abstract void fire();
    }
}
