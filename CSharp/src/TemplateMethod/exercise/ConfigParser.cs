﻿/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
using System;

namespace Tw.Teddysoft.Gof.TemplateMethod.Exercise
{
    public class ConfigParser
	{
		PersonData mPData = null;
		private String mFileName = null;

		ConfigParser(String aFileName)
		{
			mFileName = aFileName;
		}

		public PersonData doParse()
		{
			readConfigFromFile(mFileName);
			parseToken();
			mPData = buildModel();
			validate();
			return mPData;
		}

		private void readConfigFromFile(String aFileName) { }
		private void parseToken() { }
		private PersonData buildModel() { return null; }
		private void validate() { }
	}
}
