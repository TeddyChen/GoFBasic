/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.gof.ClassFactory.ans;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class WinDriveManagerTest {
	@Test
	public void createWinUSBDrive(){
		DriveManager dm = new WinDriveManager();
		Drive drive = dm.getDrive("USB", 0);
		assertEquals(WinUSBDrive.class, drive.getClass());
		assertNotEquals(WinSATADrive.class, drive.getClass());
		assertNotEquals(LinuxUSBDrive.class, drive.getClass());
		assertNotEquals(LinuxSATADrive.class, drive.getClass());
	}
	@Test
	public void createWinSATADrive(){
		DriveManager dm = new WinDriveManager();
		Drive drive = dm.getDrive("SATA", 0);
		assertEquals(WinSATADrive.class, drive.getClass());
		assertNotEquals(WinUSBDrive.class, drive.getClass());
		assertNotEquals(LinuxUSBDrive.class, drive.getClass());
		assertNotEquals(LinuxSATADrive.class, drive.getClass());
	}
	@Test
	public void createUnsupportedWinSASDrive(){
		DriveManager dm = new WinDriveManager();
		try{
			Drive drive = dm.getDrive("SAS", 0);
			fail("Infessible path.");
		}
		catch(RuntimeException e){
			assertTrue(true, "Unsupported drive type 'SAS'"
					+ " throws a RuntimeException");
		}
	}
}
