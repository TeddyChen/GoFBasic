/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.gof.TemplateMethod.ans;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ConfigParserTest {
	private ByteArrayOutputStream stream = null;
	private PrintStream printStream = null;
	@BeforeEach
	public void setUp() {
		stream = new ByteArrayOutputStream();
		printStream = new PrintStream(stream);
		System.setOut(printStream);
	}
	@AfterEach
	public void tearDown() throws IOException {
		if (null != printStream)
			printStream.close();
		if (null != stream)
			stream.close();
	}
	@Test
	public void testFileConfigParser(){
		ConfigParser fileParser = new 
				FileConfigParser("C:\\config.ini");
		PersonData fpd = fileParser.doParse();
		assertEquals("Teddy", fpd.getName());
		assertEquals(100, fpd.getHP());

		String expected = "Read config data from file: "
				+ "C:\\config.ini\n"
				+ "parseToken...\n"
				+ "validate config data built from file...\n";
		assertThat(expected)
				.isEqualToIgnoringNewLines(stream.toString());
	}
	@Test
	public void testDBConfigParser(){
		ConfigParser dbParser = new 
				DbConfigParser("http://127.0.0.1/hsql/mydb");
		PersonData dbpd = dbParser.doParse();
		assertEquals("Kay", dbpd.getName());
		assertEquals(100, dbpd.getHP());
		String expected = "Read config data from database: "
				+ "http://127.0.0.1/hsql/mydb\n"
				+ "parseToken...\n"
				+ "validate config data built from database...\n";
		assertThat(expected)
				.isEqualToIgnoringNewLines(stream.toString());
	}
}
