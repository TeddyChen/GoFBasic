/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.gof.Composite.exercise;


import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class BeforeApplyingCompositeTest {
	private ByteArrayOutputStream stream = null;
	private PrintStream printStream = null;
	@BeforeEach
	public void setUp() {
		stream = new ByteArrayOutputStream();
		printStream = new PrintStream(stream);
		System.setOut(printStream);
	}
	@AfterEach
	public void tearDown() throws IOException {
		if (null != printStream)
			printStream.close();
		if (null != stream)
			stream.close();
	}
	@Test
	public void testFiringClip() {
		Clip rootClip = new Clip();
		Clip childClip = new Clip();
		childClip.addBullet(new TraceBullet());
		childClip.addBullet(new LineBullet());
		childClip.addBullet(new TraceBullet());
		rootClip.addClip(childClip);
		rootClip.fire();
		String expected = "發射追蹤子彈.\n"
					+ "發射直線子彈.\n"
					+ "發射追蹤子彈.\n";
		assertThat(expected)
				.isEqualToIgnoringNewLines(stream.toString());
	}
	@Test
	public void testFiringBullet() {
		new LineBullet().fire();
		new TraceBullet().fire();
		new LineBullet().fire();
		String expected = "發射直線子彈.\n"
				+ "發射追蹤子彈.\n"
				+ "發射直線子彈.\n";
		assertThat(expected)
				.isEqualToIgnoringNewLines(stream.toString());
	}
	
	@Test
	public void testFiringClipWithBullet() {
		Clip rootClip = new Clip();
		Clip childClip = new Clip();
		childClip.addBullet(new TraceBullet());
		childClip.addBullet(new TraceBullet());
		childClip.addBullet(new TraceBullet());
		rootClip.addBullet(new LineBullet());
		rootClip.addClip(childClip);
		rootClip.addBullet(new LineBullet());
		rootClip.fire();
		String expected = "發射直線子彈.\n"
				+ "發射直線子彈.\n"
				+ "發射追蹤子彈.\n"
				+ "發射追蹤子彈.\n"
				+ "發射追蹤子彈.\n";
		assertThat(expected)
				.isEqualToIgnoringNewLines(stream.toString());
	}
}
