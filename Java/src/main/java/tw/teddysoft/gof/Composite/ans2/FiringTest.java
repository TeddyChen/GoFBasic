/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.gof.Composite.ans2;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class FiringTest {
	private ByteArrayOutputStream stream = null;
	private PrintStream printStream = null;
	@BeforeEach
	public void setUp() {
		stream = new ByteArrayOutputStream();
		printStream = new PrintStream(stream);
		System.setOut(printStream);
	}
	@AfterEach
	public void tearDown() throws IOException {
		if (null != printStream)
			printStream.close();
		if (null != stream)
			stream.close();
	}
	@Test
	public void testFiringClip() {
		Clip rootClip = new Clip();
		Clip childClip = new Clip();
		childClip.add(new TraceBullet());
		childClip.add(new LineBullet());
		childClip.add(new TraceBullet());
		rootClip.add(childClip);
		rootClip.fire();

		String expected = "發射追蹤子彈.\n"
					+ "發射直線子彈.\n"
					+ "發射追蹤子彈.\n";
		assertThat(expected)
				.isEqualToIgnoringNewLines(stream.toString());
	}
	@Test
	public void testFiringBullet() {
		new LineBullet().fire();
		new TraceBullet().fire();
		new LineBullet().fire();

		String expected = "發射直線子彈.\n"
				+ "發射追蹤子彈.\n"
				+ "發射直線子彈.\n";
		assertThat(expected)
				.isEqualToIgnoringNewLines(stream.toString());

	}
	@Test
	public void testFiringClipWithBullet() {
		Clip rootClip = new Clip();
		Clip childClip = new Clip();
		childClip.add(new TraceBullet());
		childClip.add(new TraceBullet());
		childClip.add(new TraceBullet());
		rootClip.add(new LineBullet());
		rootClip.add(childClip);
		rootClip.add(new LineBullet());
		rootClip.fire();

		String expected = "發射直線子彈.\n"
				+ "發射追蹤子彈.\n"
				+ "發射追蹤子彈.\n"
				+ "發射追蹤子彈.\n"
				+ "發射直線子彈.\n";
		assertThat(expected)
				.isEqualToIgnoringNewLines(stream.toString());
	}
}
