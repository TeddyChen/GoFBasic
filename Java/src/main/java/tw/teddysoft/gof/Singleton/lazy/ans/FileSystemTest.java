package tw.teddysoft.gof.Singleton.lazy.ans;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class FileSystemTest {
	@Test
	public void default_instance_is_not_null() {
		AppConfig fs1 = AppConfig.getInstance();
		assertNotNull(fs1);
	}
	@Test
	public void call_getInstance_twice_get_the_smae_instance() {
		assertEquals(AppConfig.getInstance(), AppConfig.getInstance());
	}
	@Test
	public void cannot_call_the_default_constructor()  {

		IllegalAccessException thrown = Assertions.assertThrows(IllegalAccessException.class, () -> {
			String className = "tw.teddysoft.gof.Singleton.lazy.ans.AppConfig";
			Class<?> c =  Class.forName(className);
			c.newInstance();
		});
		assertTrue(thrown.getMessage().contains("cannot access a member of class"));
	}
}
